using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data;
using System.Data.SqlClient;

namespace WebApplicationContractor.Pages.Contractors
{
    public class EditModel : PageModel
        {
        public IndividContractor individContractor = new IndividContractor();
        public String errorMessage = "";
        public String successMessage = "";
      
        
        public void OnGet()
        {
            
            try
            {
                Int32 id = Int32.Parse(Request.Query["id"]);

                String connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ContractorDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;" +
                       "ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    String sql = "SELECT * FROM Individ_contractor WHERE id = @id";
                    using (SqlCommand command = new SqlCommand(sql,connection))
                    {
                        Console.WriteLine(id);
                        Console.WriteLine(individContractor.id);

                        command.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read()) 
                            {
                                individContractor.id= "" + reader.GetInt32(0);
                                individContractor.name = reader.GetString(1);
                                individContractor.surname = reader.GetString(2);
                                individContractor.patronymic = reader.GetString(3);
                                individContractor.biin = "" + reader.GetInt64(4);
                           }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
        }

        public void OnPost() 
        {   
            individContractor.id = Request.Form["id"];
            individContractor.name = Request.Form["name"];
            individContractor.surname = Request.Form["surname"];
            individContractor.patronymic = Request.Form["patronymic"];
            individContractor.biin = Request.Form["biin"];

            

            try
            {
                String connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ContractorDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;" +
                          "ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    String sql = "UPDATE Individ_contractor" +
                                 " SET name=@name, surname=@surname, patronymic= @patronymic, biin=@biin" +
                                 " WHERE id = @id";

                    using(SqlCommand command = new SqlCommand(sql, connection))

                    {
                        command.Parameters.Add("@id", SqlDbType.Int).Value =individContractor.id;                        
                        command.Parameters.AddWithValue("@name", individContractor.name);
                        command.Parameters.AddWithValue("@surname", individContractor.surname);
                        command.Parameters.AddWithValue("@patronymic", individContractor.patronymic);
                        command.Parameters.AddWithValue("@biin", individContractor.biin);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return;
            }

            Response.Redirect("/Contractors");

        }
    }
}
