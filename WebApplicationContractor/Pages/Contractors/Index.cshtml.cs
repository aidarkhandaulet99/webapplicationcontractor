using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SqlClient;

namespace WebApplicationContractor.Pages.Contractors
{
    public class IndexModel : PageModel
    {
        public List<IndividContractor> individContractors = new List<IndividContractor>();
        public void OnGet()
        {
            try
            {
                String connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ContractorDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;" +
                    "ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    String sql = "SELECT * FROM Individ_contractor";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                IndividContractor individContractor = new IndividContractor();
                                individContractor.id = "" + reader.GetInt32(0);
                                individContractor.name = reader.GetString(1);
                                individContractor.surname = reader.GetString(2);
                                individContractor.patronymic = reader.GetString(3);
                                individContractor.biin = "" + reader.GetInt64(4);

                                individContractors.Add(individContractor);

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.ToString());
            }
        }
    }
    public class IndividContractor
    {
        public String id;
        public String name;
        public String surname;
        public String patronymic;
        public String biin;

    }
}
