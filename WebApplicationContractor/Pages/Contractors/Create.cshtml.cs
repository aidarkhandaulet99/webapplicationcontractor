using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationContractor.Pages.Contractors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SqlClient;

namespace WebApplicationContractor.Pages.Contractors
{
    public class CreateModel : PageModel
    {
        public IndividContractor individContractor = new IndividContractor();
        public String errorMessage = "";
        public String successMessage = "";
        public void OnGet()
        {
        }

        public void OnPost()
        {
            individContractor.name = Request.Form["name"];
            individContractor.surname = Request.Form["surname"];
            individContractor.patronymic = Request.Form["patronymic"];
            individContractor.biin = Request.Form["biin"];

            if (individContractor.name.Length == 0 || individContractor.surname.Length == 0 ||
                individContractor.patronymic.Length == 0 || individContractor.biin.Length == 0)
            {
                errorMessage = "All the fields are required";
                return;
            }

            //save the new contractor into the database
            try
            {
                String connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=ContractorDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;" +
                    "ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                using (SqlConnection connection = new SqlConnection(connectionString)) 
                {
                    connection.Open();
                    String sql = "INSERT INTO Individ_contractor" +
                                 "(name,surname,patronymic,biin) VALUES" +
                                 "(@name,@surname,@patronymic,@biin)";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@name", individContractor.name);
                        command.Parameters.AddWithValue("@surname", individContractor.surname);
                        command.Parameters.AddWithValue("@patronymic", individContractor.patronymic);
                        command.Parameters.AddWithValue("@biin", individContractor.biin);

                        command.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex) 
            {
                errorMessage= ex.Message;
                return;
            }

            individContractor.name = ""; individContractor.surname = ""; individContractor.patronymic = "";
            individContractor.biin = "";
            successMessage = "New Contractor added correctly";

            Response.Redirect("/Contractors");
        }
    }
}
